#include <ros/ros.h>
#include <tf/transform_broadcaster.h>

int main(int argc, char** argv){
  ros::init(argc, argv, "tc_tf");
  ros::NodeHandle n;
  ros::Rate r(100);
  tf::TransformBroadcaster broadcaster;
  tf::Quaternion quoternion;
  quoternion.setRPY(M_PI, 0, 0);//urg is reversed.    
  while(n.ok()){
    broadcaster.sendTransform(
        tf::StampedTransform(
	tf::Transform(
	tf::Quaternion(0, 0, 0, 1),
	tf::Vector3(0.0, 0.0, 0.13)),
	ros::Time::now(), "base_footprint", "base_link")
    );
    broadcaster.sendTransform(
        tf::StampedTransform(
			     tf::Transform(
					   quoternion,
					   tf::Vector3(0.13, 0.0, 0.0)
					   ),
			     ros::Time::now(), "base_link", "base_scan"
			     )
			      );
    r.sleep();
  }
}
