* 環境
    * ros-kinetic-kame
* installation
    * sudo apt-get install ros-kinetic-gmapping
    * sudo apt-get install ros-kinetic-navigation
    * sudo apt-get install ros-kinetic-urg-node
    

# tc_tf
## 概要
* url: https://bitbucket.org/ip_sakura/tc_tf
* rosbook.jpの10.3.5を元に作成。
## 設定事項
* base_linkとして、タイヤの中心軸、２つタイヤの中央を設定。
* sensor位置として、base_linkからurg設定の板を設定。

# gmapping
* http://cyberworks.cocolog-nifty.com/blog/2013/10/gmapping-12d8.html
* mapの生成の様子を見るには。
    * roscore
    * rosparam set use_sim_time true
    * wget http://pr.willowgarage.com/data/gmapping/basic_localization_stage.bag
    * rosrun gmapping slam_gmapping scan:=base_scan
    * rosbag play basic_localization_stage.bag
    * rosrun rviz rviz
    * mapを保存するには
        * rosrun map_server map_saver
    

# urg-node
* さ部屋のros本の170ページらへん。



#tf

rosbook_jpの２６３ページらへんを参照。

base_linkをタイヤとタイヤの中心にして、
軸が下から130mm(13cm)とした。

urgはたかさ１３ｃｍ（ベースリンクと平行）、前に１３ｃｍとした。


ROSでは、MKS単位、右手系を使用します。

（単位は、メートル、秒、ラジアンを使い、座標軸は右手の親指、人差し指、中指を広げ、それぞれX,Y,Zとなる向き、回転の向きは右ネジを締める方向）

なお、RVizなどで座標軸を表示する際には、X軸:R（赤）　Y軸:G（緑）　Z軸:B(青）で表します。

進行方向がｘ軸。



arduino_encorder_pulse
//left, rightのpulse数

-> odom(base_link)
/*
http://wiki.ros.org/ja/navigation/Tutorials/RobotSetup/Odom


wheel直径：380mm
wheel間距離：410mm
*/



catkin_create_pkg tc_encorder_to_odom roscpp nav_msgs std_msgs tf geometry_msgs